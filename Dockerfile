# Stage and thin the application
FROM openliberty/open-liberty:full-java15-openj9-ubi as staging

COPY --chown=1001:0 target/manage-vehicle-0.0.1-SNAPSHOT.jar \
                    /staging/fat-manage-vehicle-0.0.1-SNAPSHOT.jar




RUN springBootUtility thin \
 --sourceAppPath=/staging/fat-manage-vehicle-0.0.1-SNAPSHOT.jar \
 --targetThinAppPath=/staging/thin-manage-vehicle-0.0.1-SNAPSHOT.jar \
 --targetLibCachePath=/staging/lib.index.cache

# Build the image
FROM openliberty/open-liberty:full-java15-openj9-ubi

ARG VERSION=1.0
ARG REVISION=SNAPSHOT

LABEL \
  org.opencontainers.image.authors="Your Name" \
  org.opencontainers.image.vendor="Open Liberty" \
  org.opencontainers.image.url="local" \
  org.opencontainers.image.source="https://mywizard.vehsystempoc.com/gitlab/samuel.chee.hong.teo/manage-vehicle" \
  org.opencontainers.image.version="$VERSION" \
  org.opencontainers.image.revision="$REVISION" \
  vendor="Open Liberty" \
  name="hello app" \
  version="$VERSION-$REVISION" \
  summary="The hello application from the Spring Boot guide" \
  description="This image contains the hello application running with the Open Liberty runtime."


COPY --chown=1001:0 --from=staging /staging/lib.index.cache /lib.index.cache
COPY --chown=1001:0 --from=staging /staging/thin-manage-vehicle-0.0.1-SNAPSHOT.jar \
                    /config/dropins/spring/thin-manage-vehicle-0.0.1-SNAPSHOT.jar


COPY --chown=1001:0 src/main/liberty/config/server.xml \
                    /config/server.xml

RUN configure.sh