package com.example.manage.vehicle.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VehicleDTO {

    private Long typeKey;

    private Long vehicleId;

    @JsonProperty("vehicleAttributes")
    private List<VehicleAttributeDTO> vehicleAttributeDTOS;
}
