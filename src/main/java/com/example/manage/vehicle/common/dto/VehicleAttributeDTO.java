package com.example.manage.vehicle.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.LocalDate;
import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VehicleAttributeDTO {

    private Long attributeTypeKey;

    private String attributeTypeName;

    private String stringValue;

    private Integer integerValue;

    private Double doubleValue;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateValue;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    private ZonedDateTime zonedDateTimeValue;
}
