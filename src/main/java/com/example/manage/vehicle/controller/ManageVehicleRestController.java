package com.example.manage.vehicle.controller;

import com.example.lta.common.codes.ErrorCode;
import com.example.lta.core.exception.dto.ErrorMessageDTO;
import com.example.manage.vehicle.get.vehicles.by.id.provider.GetVehicleByIdProvider;
import com.example.manage.vehicle.get.vehicles.by.user.id.dto.GetVehiclesByUserIdOutboundPayload;
import com.example.manage.vehicle.get.vehicle.by.id.dto.GetVehicleByIdOutboundPayload;
import com.example.manage.vehicle.get.vehicles.by.user.id.exception.GetVehiclesByUserIdException;
import com.example.manage.vehicle.get.vehicles.by.user.id.provider.GetVehiclesByUserIdProvider;
import com.example.manage.vehicle.register.vehicles.dto.RegisterVehicleInboundPayload;
import com.example.manage.vehicle.register.vehicles.dto.RegisterVehicleOutboundPayload;
import com.example.manage.vehicle.register.vehicles.exception.RegisterVehicleException;
import com.example.manage.vehicle.register.vehicles.provider.RegisterVehicleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/manage-vehicle-service")
public class ManageVehicleRestController {

    @Autowired
    private RegisterVehicleProvider registerVehicleProvider;

    @Autowired
    private GetVehiclesByUserIdProvider getVehiclesByUserIdProvider;

    @Autowired
    private GetVehicleByIdProvider getVehicleByIdProvider;

    private static final String REQUEST = "request";

    @PostMapping(path = "/registerVehicle", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> registerVehicle(@RequestBody RegisterVehicleInboundPayload registerVehicleInboundPayload){
        Map<String, Object> input = new HashMap<>();
        input.put(REQUEST, registerVehicleInboundPayload);

        try{
            RegisterVehicleOutboundPayload outboundPayload = (RegisterVehicleOutboundPayload) registerVehicleProvider.execute(input);

            return ResponseEntity.status(HttpStatus.CREATED).body(outboundPayload);
        }catch(RegisterVehicleException rve){
            return ResponseEntity.badRequest().body(rve.getErrorMessage());
        }catch(Exception e){
            ErrorMessageDTO errorMessageDTO = ErrorMessageDTO.builder()
                    .code(ErrorCode.INTERNAL_SERVER_ERROR.getCode())
                    .message(e.getMessage())
                    .build();
            return ResponseEntity.internalServerError().body(errorMessageDTO);
        }
    }

    @GetMapping(path = "/getVehiclesByUserId/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getVehiclesByUserId(@PathVariable(name = "userId") Long userId){

        Map<String, Object> input = new HashMap<>();
        input.put(REQUEST, userId);

        try{
            GetVehiclesByUserIdOutboundPayload outboundPayload = (GetVehiclesByUserIdOutboundPayload) getVehiclesByUserIdProvider.execute(input);

            return ResponseEntity.ok(outboundPayload);

        }catch(GetVehiclesByUserIdException gve){
            return ResponseEntity.badRequest().body(gve.getErrorMessage());
        }catch(Exception e) {
            ErrorMessageDTO errorMessageDTO = ErrorMessageDTO.builder()
                    .code(ErrorCode.INTERNAL_SERVER_ERROR.getCode())
                    .message(e.getMessage())
                    .build();
            return ResponseEntity.internalServerError().body(errorMessageDTO);
        }
    }

    @GetMapping(path = "/getVehicleById/{vehicleId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getVehicleById(@PathVariable(name = "vehicleId") Long vehicleId){
        Map<String, Object> input = new HashMap<>();
        input.put(REQUEST, vehicleId);



        try{
            // Call the GetVehicleByIdProvider and passing in the input map.
            GetVehicleByIdOutboundPayload outboundPayload = (GetVehicleByIdOutboundPayload) getVehicleByIdProvider.execute(input);

            return ResponseEntity.ok(outboundPayload);
            // Assign to a variable with type of GetVehicleByIdOutboundPayload for the return of the provider class
            // Update the code to have a try catch clause and catch the GetVehicleByIdException and Exception.

        }catch(GetVehiclesByUserIdException gve){
            return ResponseEntity.badRequest().body(gve.getErrorMessage());
        }catch(Exception e) {
            ErrorMessageDTO errorMessageDTO = ErrorMessageDTO.builder()
                    .code(ErrorCode.INTERNAL_SERVER_ERROR.getCode())
                    .message(e.getMessage())
                    .build();
            return ResponseEntity.internalServerError().body(errorMessageDTO);
        }

        // return null;
    }
}
