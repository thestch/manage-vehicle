package com.example.manage.vehicle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.lta.core",
		"com.example.manage.vehicle"
		, "com.example.vehicle.management.domain.entity"
		, "com.example.vehicle.management.domain.repository"
		, "com.example.vehicle.management.domain.provider"})
@EntityScan("com.example.vehicle.management.domain.entity")
@EnableJpaRepositories(basePackages = {"com.example.manage.vehicle", "com.example.vehicle.management.domain.repository"})
public class ManageVehicleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManageVehicleApplication.class, args);
	}

	@Bean
	public Docket api(){
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.example.manage.vehicle.controller"))
				.paths(PathSelectors.any())
				.build();
	}

}
