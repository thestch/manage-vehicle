package com.example.manage.vehicle.get.vehicles.by.user.id.dto;

import com.example.manage.vehicle.common.dto.VehicleDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetVehiclesByUserIdOutboundPayload {

    private Long userId;

    @JsonProperty("vehicles")
    private List<VehicleDTO> vehicleDTOS;
}
