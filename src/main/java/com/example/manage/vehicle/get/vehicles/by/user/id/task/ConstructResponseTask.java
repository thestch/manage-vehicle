package com.example.manage.vehicle.get.vehicles.by.user.id.task;

import com.example.lta.core.abstracts.task.AbstractTask;
import com.example.manage.vehicle.common.dto.VehicleAttributeDTO;
import com.example.manage.vehicle.common.dto.VehicleDTO;
import com.example.manage.vehicle.get.vehicles.by.user.id.dto.GetVehiclesByUserIdOutboundPayload;
import com.example.vehicle.management.domain.entity.Vehicle;
import com.example.vehicle.management.domain.entity.VehicleAttribute;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service("GetVehiclesByUserIdConstructResponseTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ConstructResponseTask extends AbstractTask {

    private static final String REQUEST = "request";
    private static final String RESPONSE = "response";
    private static final String VEHICLES = "vehicles";

    @Override
    public Map<String, Object> process(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        List<Vehicle> vehicles = (List<Vehicle>) output.get(VEHICLES);

        Long userId = (Long) output.get(REQUEST);

        GetVehiclesByUserIdOutboundPayload outboundPayload = new GetVehiclesByUserIdOutboundPayload();

        List<VehicleDTO> vehicleDTOS = vehicles.stream().map(this::mapVehicleDTO).collect(Collectors.toList());

        outboundPayload.setUserId(userId);
        outboundPayload.setVehicleDTOS(vehicleDTOS);

        log.debug("Constructed outbound payload: {}", outboundPayload);

        output.put(RESPONSE, outboundPayload);

        return output;
    }

    private VehicleDTO mapVehicleDTO(Vehicle vehicle){
        return VehicleDTO.builder()
                .typeKey(vehicle.getVehicleType().getKey())
                .vehicleId(vehicle.getId())
                .vehicleAttributeDTOS(mapVehicleAttributeDTO(vehicle.getVehicleAttributes()))
                .build();
    }

    private List<VehicleAttributeDTO> mapVehicleAttributeDTO(List<VehicleAttribute> vehicleAttributes){
        List<VehicleAttributeDTO> vehicleAttributeDTOS = new ArrayList<>();

        if(!CollectionUtils.isEmpty(vehicleAttributes)){
            vehicleAttributes.forEach(vehicleAttribute -> {
                VehicleAttributeDTO dto = new VehicleAttributeDTO();
                dto.setAttributeTypeKey(vehicleAttribute.getVehicleAttributeType().getKey());
                dto.setAttributeTypeName(vehicleAttribute.getVehicleAttributeType().getName());
                dto.setDateValue(vehicleAttribute.getDateValue());
                dto.setDoubleValue(vehicleAttribute.getDoubleValue());
                dto.setIntegerValue(vehicleAttribute.getIntegerValue());
                dto.setStringValue(vehicleAttribute.getStringValue());
                dto.setZonedDateTimeValue(vehicleAttribute.getZonedDateTimeValue());

                vehicleAttributeDTOS.add(dto);
            });
        }

        return vehicleAttributeDTOS;
    }
}
