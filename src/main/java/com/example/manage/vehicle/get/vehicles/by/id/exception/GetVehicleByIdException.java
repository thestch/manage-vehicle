package com.example.manage.vehicle.get.vehicles.by.id.exception;

import com.example.lta.core.exception.VRLSException;

public class GetVehicleByIdException extends VRLSException {

    public GetVehicleByIdException(Long code, String message){
        super(code, message);
    }
}
