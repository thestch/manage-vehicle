package com.example.manage.vehicle.get.vehicles.by.id.task;

import com.example.lta.core.abstracts.task.AbstractTask;
import com.example.vehicle.management.domain.service.VehicleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service("GetVehicleByIdGetVehicleTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GetVehicleTask extends AbstractTask {

    @Autowired
    private VehicleService vehicleService;

    private static final String REQUEST = "request";
    private static final String VEHICLE = "vehicle";

    @Override
    public Map<String, Object> process(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        // Retrieve the vehicleId in the output map assign it to a Long variable
        // Call the vehicleService.findById and passing the vehicleId
        // If the vehicleService.findById return a null value throw GetVehicleByIdException
        // You can create a new ErrorCode in lta-common project and use it as parameter in the exception instantiation
        // If a vehicle is found then add it in the output map using the VEHICLE variable as key

        return output;
    }
}
