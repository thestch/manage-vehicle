package com.example.manage.vehicle.get.vehicles.by.user.id.task;

import com.example.lta.core.abstracts.task.AbstractTask;
import com.example.vehicle.management.domain.entity.Vehicle;
import com.example.vehicle.management.domain.service.VehicleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service("GetVehiclesByUserIdRetrieveVehiclesTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RetrieveVehiclesTask extends AbstractTask {

    private static final String REQUEST = "request";
    private static final String VEHICLES = "vehicles";

    @Autowired
    private VehicleService vehicleService;

    @Override
    public Map<String, Object> process(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        Long userId = (Long) output.get(REQUEST);

        List<Vehicle> vehicles = Optional.ofNullable(vehicleService.findByUserId(userId)).orElse(new ArrayList<>());

        log.debug("Retrieved number of vehicles: {} for userId: {}", vehicles.size(), userId);

        output.put(VEHICLES, vehicles);

        return output;
    }
}
