package com.example.manage.vehicle.get.vehicles.by.id.provider;

import com.example.lta.core.abstracts.provider.AbstractProvider;
import com.example.manage.vehicle.get.vehicles.by.id.task.ConstructResponseTask;
import com.example.manage.vehicle.get.vehicles.by.id.task.GetVehicleTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GetVehicleByIdProvider extends AbstractProvider {

    @Autowired
    private GetVehicleTask getVehicleTask;

    @Autowired
    private ConstructResponseTask constructResponseTask;

    private static final String RESPONSE = "response";

    @Override
    public Object execute(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        // Pass the output map to the getVehicleTask.process and add all to the output map.
        // Pass the output map to the constructResponseTask.process and add all to the output map.
        // Retrieve the response from the output map using the RESPONSE variable.
        // Cast the object to GetVehicleByIdOutboundPayload.
        // Return the casted object.

        return null;
    }
}
