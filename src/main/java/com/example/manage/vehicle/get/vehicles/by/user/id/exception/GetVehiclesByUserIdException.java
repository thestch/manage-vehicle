package com.example.manage.vehicle.get.vehicles.by.user.id.exception;

import com.example.lta.core.exception.VRLSException;

public class GetVehiclesByUserIdException extends VRLSException {

    public GetVehiclesByUserIdException(Long code, String message){
        super(code, message);
    }
}
