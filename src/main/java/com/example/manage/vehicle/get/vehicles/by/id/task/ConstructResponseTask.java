package com.example.manage.vehicle.get.vehicles.by.id.task;

import com.example.lta.core.abstracts.task.AbstractTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service("GetVehicleByIdConstructResponseTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ConstructResponseTask extends AbstractTask {

    private static final String VEHICLE = "vehicle";
    private static final String OUTPUT = "output";

    @Override
    public Map<String, Object> process(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        // Retrieve the vehicle from the input map
        // Create a private method that converts the Vehicle object to VehicleDTO, you can use the builder of the DTO
        // Add the converted object to the output map using the OUTPUT variable as key.

        return output;
    }
}
