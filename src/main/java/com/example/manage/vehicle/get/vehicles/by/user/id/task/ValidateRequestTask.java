package com.example.manage.vehicle.get.vehicles.by.user.id.task;

import com.example.lta.common.codes.ErrorCode;
import com.example.lta.core.abstracts.task.AbstractTask;
import com.example.manage.vehicle.get.vehicles.by.user.id.exception.GetVehiclesByUserIdException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service("GetVehiclesByUserIdValidateRequestTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ValidateRequestTask extends AbstractTask {

    private static final String REQUEST = "request";

    @Override
    public Map<String, Object> process(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        Long userId = (Long) output.get(REQUEST);

        log.debug("userId from request: {}", userId);

        if(Objects.isNull(userId)){
            throw new GetVehiclesByUserIdException(ErrorCode.USER_NOT_FOUND.getCode(), ErrorCode.USER_NOT_FOUND.getMessage());
        }

        return output;
    }
}
