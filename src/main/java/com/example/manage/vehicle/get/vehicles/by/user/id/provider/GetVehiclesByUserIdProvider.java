package com.example.manage.vehicle.get.vehicles.by.user.id.provider;

import com.example.lta.core.abstracts.provider.AbstractProvider;
import com.example.manage.vehicle.get.vehicles.by.user.id.dto.GetVehiclesByUserIdOutboundPayload;
import com.example.manage.vehicle.get.vehicles.by.user.id.task.ConstructResponseTask;
import com.example.manage.vehicle.get.vehicles.by.user.id.task.RetrieveVehiclesTask;
import com.example.manage.vehicle.get.vehicles.by.user.id.task.ValidateRequestTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class GetVehiclesByUserIdProvider extends AbstractProvider {

    @Autowired
    private ConstructResponseTask constructResponseTask;

    @Autowired
    private RetrieveVehiclesTask retrieveVehiclesTask;

    @Autowired
    private ValidateRequestTask validateRequestTask;

    private static final String RESPONSE = "response";

    @Override
    public Object execute(Map<String, Object> input){

        Map<String, Object> output = new HashMap<>(input);

        output.putAll(validateRequestTask.process(output));
        output.putAll(retrieveVehiclesTask.process(output));
        output.putAll(constructResponseTask.process(output));

        GetVehiclesByUserIdOutboundPayload outboundPayload = (GetVehiclesByUserIdOutboundPayload) output.get(RESPONSE);

        return outboundPayload;
    }
}
