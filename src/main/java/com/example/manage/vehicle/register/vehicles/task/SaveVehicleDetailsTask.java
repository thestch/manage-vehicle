package com.example.manage.vehicle.register.vehicles.task;

import com.example.lta.common.codes.ErrorCode;
import com.example.lta.core.abstracts.task.AbstractTask;
import com.example.manage.vehicle.common.dto.VehicleAttributeDTO;
import com.example.manage.vehicle.common.dto.VehicleDTO;
import com.example.manage.vehicle.register.vehicles.exception.RegisterVehicleException;
import com.example.vehicle.management.domain.entity.Vehicle;
import com.example.vehicle.management.domain.entity.VehicleAttribute;
import com.example.vehicle.management.domain.entity.VehicleAttributeType;
import com.example.vehicle.management.domain.entity.VehicleType;
import com.example.vehicle.management.domain.service.VehicleAttributeService;
import com.example.vehicle.management.domain.service.VehicleAttributeTypeService;
import com.example.vehicle.management.domain.service.VehicleService;
import com.example.vehicle.management.domain.service.VehicleTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service("RegisterVehiclesSaveVehicleDetailsTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SaveVehicleDetailsTask extends AbstractTask {

    private static final String USER_ID = "userId";
    private static final String VEHICLES = "vehicles";
    private static final String VEHICLE_IDS = "vehicleIds";

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private VehicleAttributeService vehicleAttributeService;

    @Autowired
    private VehicleAttributeTypeService vehicleAttributeTypeService;

    @Autowired
    private VehicleTypeService vehicleTypeService;

    @Override
    public Map<String, Object> process(Map<String, Object> input){

        Map<String, Object> output = new HashMap<>(input);

        Long userId = (Long) input.get(USER_ID);
        List<VehicleDTO> vehicleDTOS = (List<VehicleDTO>) input.get(VEHICLES);

        List<Long> vehicleIDs = new ArrayList<>();

        vehicleDTOS.forEach(vehicleDTO ->  {
            Vehicle vehicle = saveVehicle(userId, vehicleDTO);

            List<VehicleAttributeDTO> vehicleAttributeDTOS = vehicleDTO.getVehicleAttributeDTOS();

            vehicleAttributeDTOS.forEach(vehicleAttributeDTO -> saveVehicleAttribute(vehicle, vehicleAttributeDTO));

            vehicleIDs.add(vehicle.getId());
        });

        output.put(VEHICLE_IDS, vehicleIDs);

        return output;
    }

    private Vehicle saveVehicle(Long userId, VehicleDTO vehicleDTO){
        Vehicle vehicle = new Vehicle();

        vehicle.setUserId(userId);
        VehicleType vehicleType = vehicleTypeService.findByKey(vehicleDTO.getTypeKey());

        if(Objects.isNull(vehicleType)){
            throw new RegisterVehicleException(ErrorCode.VEHICLE_TYPE_NOT_FOUND.getCode(), ErrorCode.VEHICLE_TYPE_NOT_FOUND.getMessage());
        }

        vehicle.setVehicleType(vehicleType);

        return vehicleService.save(vehicle);
    }

    private void saveVehicleAttribute(Vehicle vehicle, VehicleAttributeDTO vehicleAttributeDTO){
        VehicleAttribute vehicleAttribute = new VehicleAttribute();

        VehicleAttributeType vehicleAttributeType = vehicleAttributeTypeService.findByKey(vehicleAttributeDTO.getAttributeTypeKey());

        if(Objects.isNull(vehicleAttributeType)){
            throw new RegisterVehicleException(ErrorCode.VEHICLE_ATTRIBUTE_TYPE_NOT_FOUND.getCode(), ErrorCode.VEHICLE_ATTRIBUTE_TYPE_NOT_FOUND.getMessage());
        }

        vehicleAttribute.setVehicle(vehicle);
        vehicleAttribute.setVehicleAttributeType(vehicleAttributeType);
        vehicleAttribute.setStringValue(vehicleAttributeDTO.getStringValue());
        vehicleAttribute.setIntegerValue(vehicleAttributeDTO.getIntegerValue());
        vehicleAttribute.setDateValue(vehicleAttributeDTO.getDateValue());
        vehicleAttribute.setDoubleValue(vehicleAttributeDTO.getDoubleValue());
        vehicleAttribute.setZonedDateTimeValue(vehicleAttributeDTO.getZonedDateTimeValue());

        vehicleAttributeService.save(vehicleAttribute);
    }
}
