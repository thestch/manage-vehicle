package com.example.manage.vehicle.register.vehicles.dto;

import com.example.manage.vehicle.common.dto.VehicleDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterVehicleInboundPayload {

    private Long userId;

    @JsonProperty("vehicles")
    private List<VehicleDTO> vehicleDTOS;
}
