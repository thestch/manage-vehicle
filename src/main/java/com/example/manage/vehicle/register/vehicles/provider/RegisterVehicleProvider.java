package com.example.manage.vehicle.register.vehicles.provider;

import com.example.lta.core.abstracts.provider.AbstractProvider;
import com.example.manage.vehicle.register.vehicles.dto.RegisterVehicleOutboundPayload;
import com.example.manage.vehicle.register.vehicles.task.ConstructResponseTask;
import com.example.manage.vehicle.register.vehicles.task.SaveVehicleDetailsTask;
import com.example.manage.vehicle.register.vehicles.task.ValidateRequestTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RegisterVehicleProvider extends AbstractProvider {

    @Autowired
    private ValidateRequestTask validateRequestTask;

    @Autowired
    private SaveVehicleDetailsTask saveVehicleDetailsTask;

    @Autowired
    private ConstructResponseTask constructResponseTask;

    private static final String RESPONSE = "response";

    @Override
    public Object execute(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        output.putAll(validateRequestTask.process(output));
        output.putAll(saveVehicleDetailsTask.process(output));
        output.putAll(constructResponseTask.process(output));

        RegisterVehicleOutboundPayload outboundPayload = (RegisterVehicleOutboundPayload) output.get(RESPONSE);

        return outboundPayload;
    }
}
