package com.example.manage.vehicle.register.vehicles.dto;

import com.example.manage.vehicle.common.dto.VehicleIDDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterVehicleOutboundPayload {

    @JsonProperty("vehicleIds")
    private List<VehicleIDDTO> vehicleIDDTOS;
}
