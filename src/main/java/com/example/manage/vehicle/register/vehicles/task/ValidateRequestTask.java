package com.example.manage.vehicle.register.vehicles.task;

import com.example.lta.common.codes.ErrorCode;
import com.example.lta.core.abstracts.task.AbstractTask;
import com.example.manage.vehicle.register.vehicles.dto.RegisterVehicleInboundPayload;
import com.example.manage.vehicle.register.vehicles.exception.RegisterVehicleException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service("RegisterVehiclesValidateRequestTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ValidateRequestTask extends AbstractTask {

    private static final String REQUEST = "request";
    private static final String USER_ID = "userId";
    private static final String VEHICLES = "vehicles";

    @Override
    public Map<String, Object> process(Map<String, Object> input){
        Map<String, Object> output = new HashMap<>(input);

        RegisterVehicleInboundPayload inboundPayload = (RegisterVehicleInboundPayload) input.get(REQUEST);

        if(Objects.isNull(inboundPayload.getUserId()) || inboundPayload.getUserId().equals(0L)){
            throw new RegisterVehicleException(ErrorCode.USER_NOT_FOUND.getCode(), ErrorCode.USER_NOT_FOUND.getMessage());
        }

        output.put(USER_ID, inboundPayload.getUserId());
        output.put(VEHICLES, inboundPayload.getVehicleDTOS());

        return output;
    }
}
