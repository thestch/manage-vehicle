package com.example.manage.vehicle.register.vehicles.exception;

import com.example.lta.core.exception.VRLSException;

public class RegisterVehicleException extends VRLSException {

    public RegisterVehicleException(Long code, String message){
        super(code, message);
    }
}
