package com.example.manage.vehicle.register.vehicles.task;

import com.example.lta.core.abstracts.task.AbstractTask;
import com.example.manage.vehicle.register.vehicles.dto.RegisterVehicleOutboundPayload;
import com.example.manage.vehicle.common.dto.VehicleIDDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service("RegisterVehiclesConstructResponseTask")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ConstructResponseTask extends AbstractTask {

    private static final String RESPONSE = "response";
    private static final String VEHICLE_IDS = "vehicleIds";

    @Override
    public Map<String, Object> process(Map<String, Object> input){

        Map<String, Object> output = new HashMap<>(input);

        List<Long> vehicleIds = (List<Long>) input.get(VEHICLE_IDS);
        RegisterVehicleOutboundPayload outboundPayload = new RegisterVehicleOutboundPayload();

        List<VehicleIDDTO> vehicleIDDTOS = vehicleIds.stream().map(VehicleIDDTO::new).collect(Collectors.toList());

        outboundPayload.setVehicleIDDTOS(vehicleIDDTOS);

        output.put(RESPONSE, outboundPayload);

        return output;
    }
}
