package com.example.manage.vehicle.register.vehicles.task;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ConstructResponseTaskTest {

    @Spy
    @InjectMocks
    private ConstructResponseTask constructResponseTask;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testProcess(){
        List<Long> vehicleIds = Collections.singletonList(1L);

        Map<String, Object> input = new HashMap<>();
        input.put("vehicleIds", vehicleIds);

        Map<String, Object> output = constructResponseTask.process(input);

        assertNotNull(output);
        assertEquals(2, output.size());
    }
}
