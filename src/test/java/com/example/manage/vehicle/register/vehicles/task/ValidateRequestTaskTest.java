package com.example.manage.vehicle.register.vehicles.task;

import com.example.manage.vehicle.register.vehicles.exception.RegisterVehicleException;
import com.example.manage.vehicle.test.helper.ManageVehicleTestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class ValidateRequestTaskTest {

    @Spy
    @InjectMocks
    private ValidateRequestTask validateRequestTask;

    private static final Long GENERIC_LONG_VALUE = 1L;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testProcess(){
        Map<String, Object> input = new HashMap<>();
        input.put("request", ManageVehicleTestHelper.buildInboundPayload(GENERIC_LONG_VALUE, ManageVehicleTestHelper.buildVehicleDTO(null)));

        Map<String, Object> output = validateRequestTask.process(input);

        assertNotNull(output);
        assertEquals(3, output.size());
    }

    @Test
    public void testProcessEmptyUserId(){
        Map<String, Object> input = new HashMap<>();
        input.put("request", ManageVehicleTestHelper.buildInboundPayload(null, ManageVehicleTestHelper.buildVehicleDTO(null)));

        assertThrows(RegisterVehicleException.class, () -> validateRequestTask.process(input));
    }

    @Test
    public void testProcessIncorrectUserId(){
        Map<String, Object> input = new HashMap<>();
        input.put("request", ManageVehicleTestHelper.buildInboundPayload(0L, ManageVehicleTestHelper.buildVehicleDTO(null)));

        assertThrows(RegisterVehicleException.class, () -> validateRequestTask.process(input));
    }
}
