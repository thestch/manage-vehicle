package com.example.manage.vehicle.register.vehicles.task;

import com.example.manage.vehicle.common.dto.VehicleAttributeDTO;
import com.example.manage.vehicle.common.dto.VehicleDTO;
import com.example.manage.vehicle.register.vehicles.exception.RegisterVehicleException;
import com.example.manage.vehicle.test.helper.ManageVehicleTestHelper;
import com.example.vehicle.management.domain.service.VehicleAttributeService;
import com.example.vehicle.management.domain.service.VehicleAttributeTypeService;
import com.example.vehicle.management.domain.service.VehicleService;
import com.example.vehicle.management.domain.service.VehicleTypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class SaveVehicleDetailsTaskTest {

    @Spy
    @InjectMocks
    private SaveVehicleDetailsTask saveVehicleDetailsTask;

    @Mock
    private VehicleService vehicleService;

    @Mock
    private VehicleAttributeService vehicleAttributeService;

    @Mock
    private VehicleAttributeTypeService vehicleAttributeTypeService;

    @Mock
    private VehicleTypeService vehicleTypeService;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);

        Mockito.when(vehicleService.save(Mockito.any())).thenReturn(ManageVehicleTestHelper.buildVehicle(
                ManageVehicleTestHelper.buildVehicleType()
        ));

        Mockito.when(vehicleAttributeService.save(Mockito.any())).thenReturn(ManageVehicleTestHelper.buildVehicleAttribute(
                ManageVehicleTestHelper.buildVehicle(ManageVehicleTestHelper.buildVehicleType()),
                ManageVehicleTestHelper.buildVehicleAttributeType(ManageVehicleTestHelper.buildVehicleAttributeTypeDataType())
        ));

        Mockito.when(vehicleAttributeTypeService.findByKey(Mockito.anyLong())).thenReturn(ManageVehicleTestHelper.buildVehicleAttributeType(
                ManageVehicleTestHelper.buildVehicleAttributeTypeDataType()
        ));

        Mockito.when(vehicleTypeService.findByKey(Mockito.anyLong())).thenReturn(ManageVehicleTestHelper.buildVehicleType());

    }

    @Test
    public void testProcess(){
        Map<String, Object> output = saveVehicleDetailsTask.process(buildInput());

        assertNotNull(output);
        assertEquals(3, output.size());

        Mockito.verify(vehicleService).save(Mockito.any());
        Mockito.verify(vehicleAttributeService).save(Mockito.any());
        Mockito.verify(vehicleTypeService).findByKey(Mockito.anyLong());
        Mockito.verify(vehicleAttributeTypeService).findByKey(Mockito.anyLong());

    }

    @Test
    public void testProcessVehicleTypeNotFound(){
        Mockito.when(vehicleTypeService.findByKey(Mockito.anyLong())).thenReturn(null);

        assertThrows(RegisterVehicleException.class, () -> saveVehicleDetailsTask.process(buildInput()));
    }

    @Test
    public void testProcessVehicleAttributeTypeNotFound(){
        Mockito.when(vehicleAttributeTypeService.findByKey(Mockito.anyLong())).thenReturn(null);

        assertThrows(RegisterVehicleException.class, () -> saveVehicleDetailsTask.process(buildInput()));
    }

    private Map<String, Object> buildInput(){
        Map<String, Object> input = new HashMap<>();

        List<VehicleAttributeDTO> vehicleAttributeDTOS = Collections.singletonList(ManageVehicleTestHelper.buildVehicleAttributeDTO());
        List<VehicleDTO> vehicleDTOS = Collections.singletonList(ManageVehicleTestHelper.buildVehicleDTO(vehicleAttributeDTOS));

        input.put("userId", 1L);
        input.put("vehicles", vehicleDTOS);

        return input;
    }

}
