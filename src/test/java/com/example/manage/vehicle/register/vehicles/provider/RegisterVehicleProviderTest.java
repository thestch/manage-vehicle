package com.example.manage.vehicle.register.vehicles.provider;

import com.example.manage.vehicle.register.vehicles.dto.RegisterVehicleOutboundPayload;
import com.example.manage.vehicle.register.vehicles.task.ConstructResponseTask;
import com.example.manage.vehicle.register.vehicles.task.SaveVehicleDetailsTask;
import com.example.manage.vehicle.register.vehicles.task.ValidateRequestTask;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RegisterVehicleProviderTest {

    @Spy
    @InjectMocks
    private RegisterVehicleProvider registerVehicleProvider;

    @Mock
    private ValidateRequestTask validateRequestTask;

    @Mock
    private SaveVehicleDetailsTask saveVehicleDetailsTask;

    @Mock
    private ConstructResponseTask constructResponseTask;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testExecute(){
        Map<String, Object> values = new HashMap<>();
        values.put("response", new RegisterVehicleOutboundPayload());

        Mockito.when(validateRequestTask.process(Mockito.anyMap())).thenReturn(values);
        Mockito.when(saveVehicleDetailsTask.process(Mockito.anyMap())).thenReturn(values);
        Mockito.when(constructResponseTask.process(Mockito.anyMap())).thenReturn(values);

        RegisterVehicleOutboundPayload output = (RegisterVehicleOutboundPayload) registerVehicleProvider.execute(values);

        assertNotNull(output);
    }
}
