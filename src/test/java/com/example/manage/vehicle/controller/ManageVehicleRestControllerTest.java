//package com.example.manage.vehicle.controller;
//
//import com.example.manage.vehicle.get.vehicles.by.user.id.dto.GetVehiclesByUserIdOutboundPayload;
//import com.example.manage.vehicle.register.vehicles.dto.RegisterVehicleInboundPayload;
//import com.example.manage.vehicle.common.dto.VehicleAttributeDTO;
//import com.example.manage.vehicle.common.dto.VehicleDTO;
//import com.example.manage.vehicle.register.vehicles.provider.RegisterVehicleProvider;
//import com.example.manage.vehicle.test.helper.ManageVehicleTestHelper;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializationFeature;
//import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
//import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
//import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
//import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
//
//import java.util.Collections;
//import java.util.List;
//
//@SpringBootTest
//@AutoConfigureMockMvc
//public class ManageVehicleRestControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean // add mock objects in the application context
//    private RegisterVehicleProvider registerVehicleProvider;
//
//    @BeforeEach
//    public void setUp(){
//        MockitoAnnotations.openMocks(this);
//
//        Mockito.when(registerVehicleProvider.execute(Mockito.anyMap())).thenReturn(ManageVehicleTestHelper.buildOutboundPayload());
//    }
//
//    @Test
//    public void testRegisterVehicle() throws Exception{
//
//        List<VehicleAttributeDTO> vehicleAttributeDTOS = Collections.singletonList(ManageVehicleTestHelper.buildVehicleAttributeDTO());
//        List<VehicleDTO> vehicleDTOS = Collections.singletonList(ManageVehicleTestHelper.buildVehicleDTO(vehicleAttributeDTOS));
//
//        RegisterVehicleInboundPayload inboundPayload = new RegisterVehicleInboundPayload();
//        inboundPayload.setUserId(1L);
//        inboundPayload.setVehicleDTOS(vehicleDTOS);
//
//        ObjectMapper objectMapper = new ObjectMapper()
//                .registerModule(new ParameterNamesModule())
//                .registerModule(new Jdk8Module())
//                .registerModule(new JavaTimeModule())
//                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//        System.out.println(objectMapper.writeValueAsString(inboundPayload));
//
//        mockMvc.perform(MockMvcRequestBuilders.post("/manage-vehicle-service/registerVehicle")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(inboundPayload))
//        ).andDo(MockMvcResultHandlers.print())
//            .andExpect(MockMvcResultMatchers.status().isCreated())
//            .andExpect(MockMvcResultMatchers.jsonPath("$.vehicleIds[0].id").value(1));
//    }
//
//    @Test
//    public void testGetVehiclesByUserId() throws Exception {
//        List<VehicleAttributeDTO> vehicleAttributeDTOS = Collections.singletonList(ManageVehicleTestHelper.buildVehicleAttributeDTO());
//        List<VehicleDTO> vehicleDTOS = Collections.singletonList(ManageVehicleTestHelper.buildVehicleDTO(vehicleAttributeDTOS));
//
//        GetVehiclesByUserIdOutboundPayload outboundPayload = GetVehiclesByUserIdOutboundPayload.builder()
//                .userId(1L)
//                .vehicleDTOS(vehicleDTOS)
//                .build();
//
//        mockMvc.perform(MockMvcRequestBuilders.get("/manage-vehicle-service/getVehiclesByUserId/1"))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.userId").value(1));
//    }
//}
