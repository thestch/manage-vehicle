package com.example.manage.vehicle.test.helper;

import com.example.manage.vehicle.common.dto.VehicleAttributeDTO;
import com.example.manage.vehicle.common.dto.VehicleDTO;
import com.example.manage.vehicle.common.dto.VehicleIDDTO;
import com.example.manage.vehicle.register.vehicles.dto.*;
import com.example.vehicle.management.domain.entity.*;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

public class ManageVehicleTestHelper {

    private static final Long GENERIC_LONG_VALUE = 1L;

    private static final String GENERIC_STRING_VALUE = "Test";

    public static Vehicle buildVehicle(VehicleType vehicleType){
        Vehicle vehicle = new Vehicle();
        vehicle.setId(GENERIC_LONG_VALUE);
        vehicle.setUserId(GENERIC_LONG_VALUE);
        vehicle.setVehicleType(vehicleType);

        return vehicle;
    }

    public static VehicleType buildVehicleType(){
        VehicleType vehicleType = new VehicleType();
        vehicleType.setKey(GENERIC_LONG_VALUE);
        vehicleType.setName(GENERIC_STRING_VALUE);
        vehicleType.setDescription(GENERIC_STRING_VALUE);

        return vehicleType;
    }

    public static VehicleAttributeType buildVehicleAttributeType(VehicleAttributeTypeDataType vehicleAttributeTypeDataType){
        VehicleAttributeType vehicleAttributeType = new VehicleAttributeType();
        vehicleAttributeType.setDescription(GENERIC_STRING_VALUE);
        vehicleAttributeType.setName(GENERIC_STRING_VALUE);
        vehicleAttributeType.setKey(GENERIC_LONG_VALUE);
        vehicleAttributeType.setVehicleAttributeTypeDataType(vehicleAttributeTypeDataType);

        return vehicleAttributeType;
    }

    public static VehicleAttributeTypeDataType buildVehicleAttributeTypeDataType(){
        VehicleAttributeTypeDataType vehicleAttributeTypeDataType = new VehicleAttributeTypeDataType();
        vehicleAttributeTypeDataType.setKey(GENERIC_LONG_VALUE);
        vehicleAttributeTypeDataType.setName(GENERIC_STRING_VALUE);
        vehicleAttributeTypeDataType.setDescription(GENERIC_STRING_VALUE);

        return vehicleAttributeTypeDataType;
    }

    public static VehicleAttribute buildVehicleAttribute(Vehicle vehicle, VehicleAttributeType vehicleAttributeType){
        VehicleAttribute vehicleAttribute = new VehicleAttribute();
        vehicleAttribute.setVehicle(vehicle);
        vehicleAttribute.setVehicleAttributeType(vehicleAttributeType);
        vehicleAttribute.setDateValue(LocalDate.now());
        vehicleAttribute.setDoubleValue(0D);
        vehicleAttribute.setIntegerValue(0);
        vehicleAttribute.setStringValue(GENERIC_STRING_VALUE);
        vehicleAttribute.setZonedDateTimeValue(ZonedDateTime.now());

        return vehicleAttribute;
    }

    public static RegisterVehicleInboundPayload buildInboundPayload(Long userId, VehicleDTO vehicleDTO){
        return RegisterVehicleInboundPayload.builder()
                .userId(userId)
                .vehicleDTOS(Collections.singletonList(vehicleDTO))
                .build();
    }

    public static VehicleDTO buildVehicleDTO(List<VehicleAttributeDTO> vehicleAttributeDTOS){
        return VehicleDTO.builder()
                .typeKey(GENERIC_LONG_VALUE)
                .vehicleAttributeDTOS(vehicleAttributeDTOS)
                .build();
    }

    public static VehicleAttributeDTO buildVehicleAttributeDTO(){
        return VehicleAttributeDTO.builder()
                .attributeTypeKey(GENERIC_LONG_VALUE)
                .dateValue(LocalDate.now())
                .zonedDateTimeValue(ZonedDateTime.now())
                .doubleValue(0D)
                .integerValue(0)
                .stringValue(GENERIC_STRING_VALUE)
                .build();
    }

    public static RegisterVehicleOutboundPayload buildOutboundPayload(){
        RegisterVehicleOutboundPayload outboundPayload = new RegisterVehicleOutboundPayload();
        outboundPayload.setVehicleIDDTOS(Collections.singletonList(
                VehicleIDDTO.builder()
                        .id(GENERIC_LONG_VALUE)
                        .build()
        ));

        return outboundPayload;
    }
}
